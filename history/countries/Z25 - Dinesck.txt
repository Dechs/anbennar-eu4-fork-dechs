government = republic
add_government_reform = merchants_reform
government_rank = 1
mercantilism = 25
primary_culture = blue_reachman
religion = regent_court
technology_group = tech_cannorian
capital = 752
national_focus = DIP
fixed_capital = 752

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1440.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}