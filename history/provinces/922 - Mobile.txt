#922

owner = A57
controller = A57
add_core = A57
culture = arannese
religion = regent_court

hre = yes

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = gnollish_minority_oppressed_small
	duration = -1
}

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}