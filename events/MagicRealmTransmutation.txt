namespace = magic_realm_transmutation


#Transmutation - Menu
country_event = {
	id = magic_realm_transmutation.0
	title = magic_realm_transmutation.0.t
	desc = magic_realm_transmutation.0.d
	picture = BIG_BOOK_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_nospells.0.a
		trigger = {
			NOT = { has_ruler_flag = transmutation_1 }
			NOT = { has_ruler_flag = transmutation_2 }
			NOT = { has_ruler_flag = transmutation_3 }
		}
		ai_chance = {
			factor = 50
		}
		
		country_event = { id = magic_realm.0 days = 0 }
	}
	
	option = {	# transmutation.1 - [1] Plant Growth
		name = magic_realm_transmutation.1.t
		trigger = {
			has_ruler_flag = transmutation_1
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				province_with_farm_goods = no
			}
			modifier = {
				factor = 2
				personality = ai_capitalist
			}
		}
		
		country_event = { id = magic_realm_transmutation.1 days = 0 }
		
		custom_tooltip = tooltip_option_transmutation_1
	}
	
	option = {	# transmutation.1 - [2] Enchance Ability
		name = magic_realm_transmutation.2.t
		trigger = {
			has_ruler_flag = transmutation_2
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				personality = ai_diplomat
			}
			modifier = {
				factor = 2
				OR = {
					NOT = { adm = 5 }
					NOT = { dip = 5 }
					NOT = { mil = 5 }
				}
			}
		}
		
		country_event = { id = magic_realm_transmutation.2 days = 0 }
		
		custom_tooltip = tooltip_option_transmutation_2
	}
	
	option = {	# transmutation.4 - [2] Rite of Conception
		name = magic_realm_transmutation.4.t
		trigger = {
			has_ruler_flag = transmutation_2
			has_ruler_flag = abjuration_1
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 5
				has_heir = no
			}
			modifier = {
				factor = 2
				has_heir = yes
				NOT = { heir_has_personality = mage_personality }
			}
		}
		
		country_event = { id = magic_realm_transmutation.4 days = 0 }
		
		custom_tooltip = tooltip_option_transmutation_2
		custom_tooltip = tooltip_option_abjuration_1
	}
	
	option = {	# transmutation.3 - [3] Create Homunculus
		name = magic_realm_transmutation.3.t
		trigger = {
			has_ruler_flag = transmutation_3
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				has_ruler_flag = magic_project_homunculus_started
			}
		}
		
		country_event = { id = magic_realm_transmutation.3 days = 0 }
		
		custom_tooltip = tooltip_option_transmutation_3
	}
	
	option = {
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm.0 days = 0 }
	}
}

# 1 - Encourage Plant Growth
country_event = {
	id = magic_realm_transmutation.1
	title = magic_realm_transmutation.1.t
	desc = magic_realm_transmutation.1.d
	picture = FARMING_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_transmutation.1.cost
		trigger = {
			OR = {
				NOT = { adm_power = 60 }
			}
		}
		country_event = { id = magic_realm_transmutation.0 days = 0 }
	}

	option = {	#Cast Plant Growth
		name = magic_realm_transmutation.1.a
		trigger = {
			has_ruler_flag = transmutation_1
		}
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_adm_power = -30
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = { has_ruler_flag = transmutation_2 }
			random_owned_province = {
				limit = { 
					province_with_farm_goods = yes
					is_in_capital_area = yes
					NOT = { has_province_modifier = magic_realm_transmutation_plant_growth_1 }
					NOT = { has_province_modifier = magic_realm_transmutation_plant_growth_2 }
					NOT = { has_province_modifier = magic_estate_plant_growth }
				}
				add_province_modifier = { 
					name = magic_realm_transmutation_plant_growth_2
					duration = 365
				}
			}
		}
		else = {
			random_owned_province = {
				limit = { 
					province_with_farm_goods = yes
					is_in_capital_area = yes
					NOT = { has_province_modifier = magic_realm_transmutation_plant_growth_1 }
					NOT = { has_province_modifier = magic_realm_transmutation_plant_growth_2 }
					NOT = { has_province_modifier = magic_estate_plant_growth }
				}
				add_province_modifier = { 
					name = magic_realm_transmutation_plant_growth_1
					duration = 365
				}
			}
		}

	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_transmutation.0 days = 0 }
	}
}

# 2 - Enchance Ability (its a temp stat increases) WHAT IF ITS PERMANENT? see notes
country_event = {
	id = magic_realm_transmutation.2
	title = magic_realm_transmutation.2.t
	desc = magic_realm_transmutation.2.d
	picture = BUDDHA_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_unavailable.0.a
		trigger = {
			OR = {
				has_ruler_modifier = magic_cooldown_transmutation_enchance_ability
			}
		}
		custom_tooltip = tooltip_magic_realm_unavailable_cooldown
	}
	
	option = {	#Cast Dame's Wisdom (adm)
		name = magic_realm_transmutation.2.a
		trigger = {
			has_ruler_flag = transmutation_2
			NOT = { has_ruler_modifier = magic_cooldown_transmutation_enchance_ability }
			
			NOT = { adm = 6 }
			
			enhance_ability_cost_adm_trigger = yes	#makes it available if country can afford it
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				NOT = { adm = 4 }
			}
		}
		
		#Cost
		if = {
			limit = {
				OR = {
					AND = {
						adm = 0
						NOT = { adm = 1 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_adm_power = -100
			
			random_list = {
				75 = {
					change_adm = 1
				}
				25 = {
				
				}
			}
		}		
		else_if = {
			limit = {
				OR = {
					AND = {
						adm = 0
						NOT = { adm = 1 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						adm = 1
						NOT = { adm = 2 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_adm_power = -100
			
			random_list = {
				70 = {
					change_adm = 1
				}
				30 = {
				
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						adm = 1
						NOT = { adm = 2 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						adm = 2
						NOT = { adm = 3 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_adm_power = -100
			random_list = {
				70 = {
					change_adm = 1
				}
				20 = {
				
				}
				10 = {
					change_adm = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						adm = 2
						NOT = { adm = 3 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						adm = 3
						NOT = { adm = 4 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_adm_power = -200
			random_list = {
				60 = {
					change_adm = 1
				}
				30 = {
				
				}
				10 = {
					change_adm = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						adm = 3
						NOT = { adm = 4 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						adm = 4
						NOT = { adm = 5 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_adm_power = -300
			random_list = {
				50 = {
					change_adm = 1
				}
				40 = {
				
				}
				10 = {
					change_adm = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						adm = 4
						NOT = { adm = 5 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						adm = 5
						NOT = { adm = 6 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_adm_power = -400
			random_list = {
				40 = {
					change_adm = 1
				}
				40 = {
				
				}
				20 = {
					change_adm = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						adm = 5
						NOT = { adm = 6 }
						has_ruler_flag = transmutation_2
					}
				}
			}
			add_adm_power = -500
			random_list = {
				30 = {
					change_adm = 1
				}
				40 = {
				
				}
				30 = {
					change_adm = -1
				}
			}
		}
		
		#Effect
		magic_casted_spell = yes
		
		add_ruler_modifier = {
			name = magic_cooldown_transmutation_enchance_ability
			duration = 1825
			hidden = yes
		}
	}
	
	option = {	#Cast Minara's Tongue (dip)
		name = magic_realm_transmutation.2.b
		trigger = {
			has_ruler_flag = transmutation_2
			NOT = { has_ruler_modifier = magic_cooldown_transmutation_enchance_ability }
			
			NOT = { dip = 6 }
			
			enhance_ability_cost_dip_trigger = yes	#makes it available if country can afford it
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				NOT = { dip = 4 }
			}
		}
		
		#Cost
		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 1 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_dip_power = -100
			
			random_list = {
				75 = {
					change_dip = 1
				}
				25 = {
				
				}
			}
		}		
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 1 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						dip = 1
						NOT = { dip = 2 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_dip_power = -100
			
			random_list = {
				70 = {
					change_dip = 1
				}
				30 = {
				
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 1
						NOT = { dip = 2 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						dip = 2
						NOT = { dip = 3 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_dip_power = -100
			random_list = {
				70 = {
					change_dip = 1
				}
				20 = {
				
				}
				10 = {
					change_dip = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 3 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						dip = 3
						NOT = { dip = 4 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_dip_power = -200
			random_list = {
				60 = {
					change_dip = 1
				}
				30 = {
				
				}
				10 = {
					change_dip = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 3
						NOT = { dip = 4 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						dip = 4
						NOT = { dip = 5 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_dip_power = -300
			random_list = {
				50 = {
					change_dip = 1
				}
				40 = {
				
				}
				10 = {
					change_dip = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 5 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						dip = 5
						NOT = { dip = 6 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_dip_power = -400
			random_list = {
				40 = {
					change_dip = 1
				}
				40 = {
				
				}
				20 = {
					change_dip = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 5
						NOT = { dip = 6 }
						has_ruler_flag = transmutation_2
					}
				}
			}
			add_dip_power = -500
			random_list = {
				30 = {
					change_dip = 1
				}
				40 = {
				
				}
				30 = {
					change_dip = -1
				}
			}
		}
		
		#Effect
		magic_casted_spell = yes
		
		add_ruler_modifier = {
			name = magic_cooldown_transmutation_enchance_ability
			duration = 1825
			hidden = yes
		}
	}
	
	option = {	#Cast Adean's Might (mil)
		name = magic_realm_transmutation.2.a
		trigger = {
			has_ruler_flag = transmutation_2
			NOT = { has_ruler_modifier = magic_cooldown_transmutation_enchance_ability }
			
			NOT = { mil = 6 }
			
			enhance_ability_cost_mil_trigger = yes	#makes it available if country can afford it
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				NOT = { mil = 4 }
			}
		}
		
		#Cost
		if = {
			limit = {
				OR = {
					AND = {
						mil = 0
						NOT = { mil = 1 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_mil_power = -100
			
			random_list = {
				75 = {
					change_mil = 1
				}
				25 = {
				
				}
			}
		}		
		else_if = {
			limit = {
				OR = {
					AND = {
						mil = 0
						NOT = { mil = 1 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						mil = 1
						NOT = { mil = 2 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_mil_power = -100
			
			random_list = {
				70 = {
					change_mil = 1
				}
				30 = {
				
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						mil = 1
						NOT = { mil = 2 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						mil = 2
						NOT = { mil = 3 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_mil_power = -100
			random_list = {
				70 = {
					change_mil = 1
				}
				20 = {
				
				}
				10 = {
					change_mil = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						mil = 2
						NOT = { mil = 3 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						mil = 3
						NOT = { mil = 4 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_mil_power = -200
			random_list = {
				60 = {
					change_mil = 1
				}
				30 = {
				
				}
				10 = {
					change_mil = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						mil = 3
						NOT = { mil = 4 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						mil = 4
						NOT = { mil = 5 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_mil_power = -300
			random_list = {
				50 = {
					change_mil = 1
				}
				40 = {
				
				}
				10 = {
					change_mil = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						mil = 4
						NOT = { mil = 5 }
						has_ruler_flag = transmutation_2
					}
					AND = {
						mil = 5
						NOT = { mil = 6 }
						has_ruler_flag = transmutation_3
					}
				}
			}
			add_mil_power = -400
			random_list = {
				40 = {
					change_mil = 1
				}
				40 = {
				
				}
				20 = {
					change_mil = -1
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						mil = 5
						NOT = { mil = 6 }
						has_ruler_flag = transmutation_2
					}
				}
			}
			add_mil_power = -500
			random_list = {
				30 = {
					change_mil = 1
				}
				40 = {
				
				}
				30 = {
					change_mil = -1
				}
			}
		}
		
		#Effect
		magic_casted_spell = yes
		
		add_ruler_modifier = {
			name = magic_cooldown_transmutation_enchance_ability
			duration = 1825
			hidden = yes
		}
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_transmutation.0 days = 0 }
	}
}

# 3 - Create Homunculus
country_event = {
	id = magic_realm_transmutation.3
	title = magic_realm_transmutation.3.t
	desc = magic_realm_transmutation.3.d
	picture = wihgfx_FEMALES_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_project_in_progress.0.a
		trigger = {
			has_ruler_flag = magic_project_homunculus_started
		}
		custom_tooltip = tooltip_magic_realm_unavailable_project_in_progress
	}
	
	option = {	#Create Homunculis
		name = magic_realm_transmutation.3.a
		trigger = {
			has_ruler_flag = transmutation_3
			NOT = { has_ruler_flag = magic_project_homunculus_started }
			NOT = { has_ruler_flag = magic_project_homunculus_complete }
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				NOT = { adm_power = 50 }
				NOT = { dip_power = 50 }
				NOT = { mil_power = 50 }
			}
		}
		
		#Cost
		add_adm_power = -50
		add_dip_power = -50
		add_mil_power = -50
		
		#Effect
		magic_casted_spell = yes
		
		set_ruler_flag = magic_project_homunculus_started
		custom_tooltip = tooltip_magic_project_start
		
		increase_witch_king_points_medium = yes
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_transmutation.0 days = 0 }
	}
}

# 2 - Rite of Conception
country_event = {
	id = magic_realm_transmutation.4
	title = magic_realm_transmutation.4.t
	desc = magic_realm_transmutation.4.d
	picture = NEW_HEIR_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_transmutation.4.cost
		trigger = {
			OR = {
				NOT = { adm_power = 100 }
			}
		}
		country_event = { id = magic_realm_transmutation.0 days = 0 }
	}

	option = {	#Perform the Rite (mistress)
		name = magic_realm_transmutation.4.a
		trigger = {
			has_ruler_flag = transmutation_2
			has_ruler_flag = abjuration_1
			has_consort = no
			
			has_government_attribute = heir
			NOT = { has_reform = adventurer_reform }
			
			#Cost
			adm_power = 100
		}
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_adm_power = -100
		add_legitimacy = -20
		add_prestige = -10
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				ruler_has_personality = mage_personality
				consort_has_personality = mage_personality
			}
			random_list = {
				25 = {
					define_heir = {
						dynasty = ROOT
						claim = 20	
						no_consort_with_heir = yes
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				70 = {
					
				}
				5 = {
					kill_ruler = yes
				}
			}
		}
		else = {
			random_list = {
				10 = {
					define_heir = {
						dynasty = ROOT
						claim = 20		
						no_consort_with_heir = yes
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				85 = {
					
				}
				5 = {
					kill_ruler = yes
				}
			}
		}

	}
	
	
	option = {	#Perform the Rite (wife)
		name = magic_realm_transmutation.4.b
		trigger = {
			has_ruler_flag = transmutation_2
			has_ruler_flag = abjuration_1
			has_consort = yes
			
			has_government_attribute = heir
			NOT = { has_reform = adventurer_reform }
			
			#Cost
			adm_power = 100
		}
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_adm_power = -100
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				AND = {
					ruler_has_personality = mage_personality
					consort_has_personality = mage_personality
				}
			}
			random_list = {
				50 = {
					define_heir = {
						dynasty = ROOT
						claim = 100					
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				40 = {
					
				}
				5 = {
					kill_ruler = yes
				}
				5 = {
					remove_consort = yes
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					ruler_has_personality = mage_personality
					consort_has_personality = mage_personality
				}
			}
			random_list = {
				25 = {
					define_heir = {
						dynasty = ROOT
						claim = 100		
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				60 = {
					
				}
				5 = {
					kill_ruler = yes
				}
				10 = {
					remove_consort = yes
				}
			}
		}
		else = {
			random_list = {
				10 = {
					define_heir = {
						dynasty = ROOT
						claim = 100
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				70 = {
					
				}
				5 = {
					kill_ruler = yes
				}
				15 = {
					remove_consort = yes
				}
			}
		}

	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_transmutation.0 days = 0 }
	}
}