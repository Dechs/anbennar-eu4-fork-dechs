# No previous file for Shigatse
owner = F27
controller = F27
add_core = F27
culture = firanyan_harpy
religion = the_hunt


base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = orcish_minority_coexisting_large
	duration = -1
}