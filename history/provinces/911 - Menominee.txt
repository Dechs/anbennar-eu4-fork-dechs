#911 - Menominee

owner = A62
controller = A62
add_core = A62
culture = esmari
religion = regent_court

hre = yes

base_tax = 7
base_production = 10
base_manpower = 4

trade_goods = iron
capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = vanbury_steel_foundry
	duration = -1
}

add_permanent_province_modifier = {
	name = dwarven_minority_integrated_large
	duration = -1
}