#325 - Kastamonu

owner = A72
controller = A72
add_core = A72
culture = arannese
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 1

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_goblin
discovered_by = tech_bulwari