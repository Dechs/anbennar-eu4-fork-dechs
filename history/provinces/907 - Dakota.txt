#907 - Dakota

owner = A30
controller = A30
add_core = A30
culture = wexonard
religion = regent_court

hre = yes

base_tax = 4
base_production = 3
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish