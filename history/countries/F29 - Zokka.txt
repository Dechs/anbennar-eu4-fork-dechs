government = tribal
add_government_reform = roaming_horde
government_rank = 1
primary_culture = east_sandfang_gnoll
religion = xhazobkult
technology_group = tech_gnollish
national_focus = DIP
capital = 571

1425.6.12 = {
	monarch = {
		name = "Zokka"
		dynasty = "Devourer-of-Suns"
		birth_date = 1401.9.2
		adm = 1
		dip = 3
		mil = 5
	}
}