#303 - Saratov

owner = A30
controller = A30
add_core = A30
culture = wexonard
religion = regent_court

hre = yes

base_tax = 3
base_production = 7
base_manpower = 3

trade_goods = iron

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
