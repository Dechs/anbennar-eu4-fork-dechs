#464 - Astrakhan

owner = F01
controller = F01
add_core = F01
add_core = F04
culture = kheteratan
religion = khetist

hre = no

base_tax = 4
base_production = 6
base_manpower = 5

trade_goods = grain
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari