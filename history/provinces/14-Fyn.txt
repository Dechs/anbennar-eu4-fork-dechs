#14 - Fyn | The Pearls

owner = A11
controller = A11
add_core = A11
culture = pearlsedger
religion = regent_court

hre = yes

base_tax = 3
base_production = 6
base_manpower = 1

trade_goods = gems

capital = "The Dame's Necklace"

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish

add_permanent_province_modifier = {
	name = damespearl_toll
	duration = -1
}