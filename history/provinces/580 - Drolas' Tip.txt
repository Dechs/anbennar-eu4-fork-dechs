# No previous file for Drolas' Tip
owner = F32
controller = F32
add_core = F32
culture = brasanni
religion = bulwari_sun_cult

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = fish
capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = elven_minority_integrated_small
	duration = -1
}