#102 - Nice

owner = A02
controller = A02
add_core = A02
culture = low_lorentish
religion = regent_court
hre = no
base_tax = 6
base_production = 6
trade_goods = fish
center_of_trade = 1
base_manpower = 4
capital = ""
is_city = yes



discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

