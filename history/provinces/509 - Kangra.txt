#509 - Mestikisorotin (moontent)

owner = F12
controller = F12
add_core = F12
culture = forest_goblin
religion = goblinic_shamanism


base_tax = 3
base_production = 2
base_manpower = 1

trade_goods = naval_supplies

capital = ""

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari