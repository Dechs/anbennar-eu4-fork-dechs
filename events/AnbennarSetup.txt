
namespace = anbennar_setup

#Country Setup for Monstrous Opinions
country_event = {
	id =  anbennar_setup.1
	title =  anbennar_setup.1.t
	desc =  anbennar_setup.1.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	
	trigger = {
		#NOT = { is_year = 1444 }
		NOT = { is_year = 1650 }
	}
	
	option = {		
		name = "anbennar_setup.1.a"
		
        if = {
            limit = {
                has_country_modifier = monstrous_nation
            }
			every_country = {	#If you're a monstrous nation you like other monstrous nations
				limit = {
					has_country_modifier = monstrous_nation
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = both_monstrous } 
					}
				}
				add_opinion = { who = ROOT modifier = both_monstrous }
			}
			every_country = {	#If you're not a monstrous nation you hate monstrous nations
				limit = {
					NOT = { has_country_modifier = monstrous_nation }
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					}
				}
				add_opinion = { who = ROOT modifier = root_monstrous }
			}
		}
		
        if = {
            limit = {
                NOT = { has_country_modifier = monstrous_nation }
            }
			every_country = {	#If you're a monstrous nation you like other monstrous nations
				limit = {
					has_country_modifier = monstrous_nation
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					}
				}
				add_opinion = { who = ROOT modifier = root_monstrous }
			}
		}
	}
}

#Country Setup for Lilac Wars Parties
country_event = {
	id =  anbennar_setup.2
	title =  anbennar_setup.2.t
	desc =  anbennar_setup.2.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	
	trigger = {
		AND = {
			is_year = 1444
			NOT = { is_year = 1445 }
		}
		#NOT = { is_year = 1444 }
	}

	
	option = {		
		name = "anbennar_setup.2.a"
		
		# If you're a member of the Rose Party
        if = {
            limit = {
                has_country_flag = lilac_wars_rose_party
            }
			every_country = {	# Rose Party members like each other
				limit = {
					has_country_flag = lilac_wars_rose_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_rose_party_member } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_rose_party_member }
			}
			every_country = {	# Rose Party members dislike Moon Party
				limit = {
					has_country_flag = lilac_wars_moon_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_rose_party_enemy } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_rose_party_enemy }
			}
		}
		
		# If you're a member of the Moon Party
        if = {
            limit = {
                has_country_flag = lilac_wars_moon_party
            }
			every_country = {	# Rose Party members like each other
				limit = {
					has_country_flag = lilac_wars_moon_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_moon_party_member } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_moon_party_member }
			}
			every_country = {	# Rose Party members dislike Moon Party
				limit = {
					has_country_flag = lilac_wars_rose_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_moon_party_enemy } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_moon_party_enemy }
			}
		}
	}
}

#Starting discovered territories
country_event = {
	id =  anbennar_setup.3
	title =  anbennar_setup.3.t
	desc =  anbennar_setup.3.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes

	
	option = {		
		name = "anbennar_setup.3.a"
		
		#civilized boyos
        if = {
            limit = {
				technology_group = tech_cannorian
            }
			escann_superregion = {
				discover_country = ROOT
			}
			bulwar_superregion = {
				discover_country = ROOT
			}
			western_cannor_superregion = {
				discover_country = ROOT
			}
			gerudia_superregion = {
				discover_country = ROOT
			}
		}
		
		#goblins and orcs
        if = {
            limit = {
				OR = {
					technology_group = tech_goblin
					technology_group = tech_orcish
				}
            }
			escann_superregion = {
				discover_country = ROOT
			}
			bulwar_superregion = {
				discover_country = ROOT
			}
			gerudia_superregion = {
				discover_country = ROOT
			}
			giants_grave_sea_region = {
				discover_country = ROOT
			}
		}
		
        if = {
            limit = {
				capital_scope = {
					superregion = escann_superregion
				}
            }
			escann_superregion = {
				discover_country = ROOT
			}
			forlorn_vale_region = {
				discover_country = ROOT
			}
			1326 = {
				discover_country = ROOT
			}
		}
		
        if = {
            limit = {
				OR = {
					capital_scope = {
						superregion = gerudia_superregion
					}
				}
            }
			gerudia_superregion = {
				discover_country = ROOT
			}
		}
		
		# bulwar and salahad
        if = {
            limit = {
				OR = {
					technology_group = tech_bulwari
					technology_group = tech_salahadesi
				}
            }
			western_cannor_superregion = {
				discover_country = ROOT
			}
			dameshead_sea_region = {
				discover_country = ROOT
			}
			bulwar_superregion = {
				discover_country = ROOT
			}
			salahad_superregion = {
				discover_country = ROOT
			}
		}
		
		#taychendi
        if = {
            limit = {
				OR = {
					culture_group = taychendi_ruinborn_elf
					culture_group = kheionai_ruinborn_elf
					culture_group = eltibhari_ruinborn_elf
					culture_group = devandi_ruinborn_elf
				}
            }
			greater_taychend_superregion = {
				discover_country = ROOT
			}
			kheionai_superregion = {
				discover_country = ROOT
			}
			cleaved_sea_area = {
				discover_country = ROOT
			}
			south_aelantir_coast_area = {
				discover_country = ROOT
			}
		}
		
		#eordand
        if = {
            limit = {
				technology_group = tech_eordand
            }
			eordand_superregion = {
				discover_country = ROOT
			}
		}
		
		#ynn
        if = {
            limit = {
				technology_group = tech_ynnic
            }
			sarda_region = {
				discover_country = ROOT
			}
			dolindha_region = {
				discover_country = ROOT
			}
			rzenta_region = {
				discover_country = ROOT
			}
		}
	
	}
}

#Pragmatic Sanction and prob other misc initializers
country_event = {
	id =  anbennar_setup.4
	title =  anbennar_setup.4.t
	desc =  anbennar_setup.4.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	
	# trigger = {
		# NOT = { is_year = 1444 }
	# }
	
	option = {		
		name = "anbennar_setup.4.a"
		
		#civilized boyos
       set_allow_female_emperor = yes
	   
	   if = {
			limit = {
				has_dlc = "Art of War"
			}
			set_hre_religion = regent_court
			set_hre_heretic_religion = corinite
	   }
	}
}

#Remove the Monstrous opinion
country_event = {
	id =  anbennar_setup.5
	title =  anbennar_setup.5.t
	desc =  anbennar_setup.5.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	fire_only_once = yes
	hidden = yes
	#is_triggered_only = yes
	
	trigger = {
		is_year = 1651
	}
	
	option = {		
		name = "anbennar_setup.5.a"
		
		every_country = {	#If you're a monstrous nation you like other monstrous nations
			remove_opinion = { who = ROOT modifier = both_monstrous }
			remove_opinion = { who = ROOT modifier = root_monstrous }
		}
	}
			# every_country = {	#If you're not a monstrous nation you hate monstrous nations
				# limit = {
					# #NOT = { has_country_modifier = monstrous_nation }
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
			# every_country = {	#If you're a monstrous nation you like other monstrous nations
				# limit = {
					# has_country_modifier = monstrous_nation
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
		
        # if = {
            # limit = {
                # has_country_modifier = monstrous_nation
            # }
			# every_country = {	#If you're a monstrous nation you like other monstrous nations
				# limit = {
					# #has_country_modifier = monstrous_nation
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = both_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = both_monstrous }
			# }
			# every_country = {	#If you're not a monstrous nation you hate monstrous nations
				# limit = {
					# #NOT = { has_country_modifier = monstrous_nation }
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
		# }
		
        # if = {
            # limit = {
                # NOT = { has_country_modifier = monstrous_nation }
            # }
			# every_country = {	#If you're a monstrous nation you like other monstrous nations
				# limit = {
					# has_country_modifier = monstrous_nation
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
		# }
	#}
}


#Base Setup for Mage Type
country_event = {
	id =  anbennar_setup.6
	title =  anbennar_setup.6.t
	desc =  anbennar_setup.6.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes

	
	option = {
		name = "anbennar_setup.6.a"
		every_country = {
			limit = {
				has_country_flag = mage_organization_decentralized_flag
				NOT = { has_country_modifier = mage_organization_decentralized }
			}
			add_country_modifier = { 
				name = mage_organization_decentralized 
				duration = -1 
				desc = mage_organization_decentralized_tooltip
			}
		}
		every_country = {	# Rose Party members like each other
			limit = {
				has_country_flag = mage_organization_centralized_flag
				NOT = { has_country_modifier = mage_organization_centralized }
			}
			add_country_modifier = { 
				name = mage_organization_centralized 
				duration = -1 
				desc = mage_organization_centralized_desc
			}
		}
		every_country = {	# Rose Party members like each other
			limit = {
				has_country_flag = mage_organization_magisterium_flag
				NOT = { has_country_modifier = mage_organization_magisterium }
			}
			add_country_modifier = { 
				name = mage_organization_magisterium 
				duration = -1 
				desc = mage_organization_magisterium_desc
			}
		}
	}
}

#Country Setup for Evil Opinions
country_event = {
	id =  anbennar_setup.7
	title =  anbennar_setup.7.t
	desc =  anbennar_setup.7.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	
	option = {		
		name = "anbennar_setup.1.a"
		
        if = {
            limit = {
                has_country_modifier = witch_king_modifier
            }
			every_country = {	#If you're a monstrous nation you like other monstrous nations
				limit = {
					has_country_modifier = witch_king_modifier
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = both_evil } 
					}
				}
				add_opinion = { who = ROOT modifier = both_evil }
			}
			every_country = {	#If you're not a monstrous nation you hate monstrous nations
				limit = {
					NOT = { has_country_modifier = witch_king_modifier }
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = root_evil } 
					}
				}
				add_opinion = { who = ROOT modifier = root_evil }
			}
		}
		
        if = {
            limit = {
                NOT = { has_country_modifier = witch_king_modifier }
            }
			every_country = {	#If you're a monstrous nation you like other monstrous nations
				limit = {
					has_country_modifier = witch_king_modifier
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = root_evil } 
					}
				}
				add_opinion = { who = ROOT modifier = root_evil }
			}
		}
	}
}

#Remove the Evil opinion
country_event = {
	id =  anbennar_setup.8
	title =  anbennar_setup.8.t
	desc =  anbennar_setup.8.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	hidden = yes
	is_triggered_only = yes
	
	option = {		
		name = "anbennar_setup.5.a"
		
		every_country = {	#If you're a monstrous nation you like other monstrous nations
			remove_opinion = { who = ROOT modifier = both_evil }
			remove_opinion = { who = ROOT modifier = root_evil }
		}
	}
			# every_country = {	#If you're not a monstrous nation you hate monstrous nations
				# limit = {
					# #NOT = { has_country_modifier = monstrous_nation }
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
			# every_country = {	#If you're a monstrous nation you like other monstrous nations
				# limit = {
					# has_country_modifier = monstrous_nation
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
		
        # if = {
            # limit = {
                # has_country_modifier = monstrous_nation
            # }
			# every_country = {	#If you're a monstrous nation you like other monstrous nations
				# limit = {
					# #has_country_modifier = monstrous_nation
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = both_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = both_monstrous }
			# }
			# every_country = {	#If you're not a monstrous nation you hate monstrous nations
				# limit = {
					# #NOT = { has_country_modifier = monstrous_nation }
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
		# }
		
        # if = {
            # limit = {
                # NOT = { has_country_modifier = monstrous_nation }
            # }
			# every_country = {	#If you're a monstrous nation you like other monstrous nations
				# limit = {
					# has_country_modifier = monstrous_nation
					# NOT = { 
						# has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					# }
				# }
				# remove_opinion = { who = ROOT modifier = root_monstrous }
			# }
		# }
	#}
}