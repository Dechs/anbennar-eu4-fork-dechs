# No previous file for Auvergne
owner = A06
controller = A06
add_core = A06
culture = cliff_gnome
religion = the_thought

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = fish
capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
