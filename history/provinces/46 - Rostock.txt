#46 - Rostock | Jewelpoint

owner = A11
controller = A11
add_core = A11
culture = pearlsedger
religion = regent_court

hre = yes

base_tax = 4
base_production = 6
base_manpower = 2

trade_goods = gems #Jewelcrafters

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish

add_permanent_province_modifier = {
	name = pearlywine_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = dwarven_minority_coexisting_small
	duration = -1
}