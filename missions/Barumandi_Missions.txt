barumand_corvurian_branch = {
	slot = 2
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		tag = Z53
	}
	
	Z53_conquer_marrhold = {
		icon = mission_conquer_50_development
		generic = no
		ai = yes
		position = 2
		
		required_missions = {	Z53_grand_gathering	}
		
		provinces_to_highlight = {
			area = marrhold_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			NOT = {
				exists = B36
			}
			marrhold_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT	
			}
		}
		
		effect = {
			add_prestige = 10
			add_country_modifier = {
				name = barumand_consolidation_of_barumand
				duration = 9125
			}
		}
	}
	
	Z53_secure_corvurian_border = {
		icon = mission_build_up_to_force_limit
		generic = no
		ai = yes
		position = 3
		
		required_missions = {	Z53_conquer_marrhold	}
		
		provinces_to_highlight = {
			area = dostans_way_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			dostans_way_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT	
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_develop_fortifications
				duration = 7300
			}
		}
	}
	
	Z53_fortify_corvurian_border = {
		icon = mission_fortify_rajputana
		generic = no
		ai = yes
		position = 4
		
		required_missions = {	Z53_secure_corvurian_border	}
		
		provinces_to_highlight = {
			province_id = 803
			OR = {
				NOT = { country_or_non_sovereign_subject_holds = ROOT  }
				NOT = { fort_level = 2 }
			}
		}
		
		trigger = {
			518 = {
				fort_level = 2
				owned_by = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_new_castles
				duration = 9125
			}
			ROOT = {
				add_casus_belli = {
					type = cb_vassalize_mission
					months = 300
					target = A59
				}
			}
		}
	}
	
	Z53_subjugate_corvuria = {
		icon = mission_conquer_50_development
		generic = no
		ai = yes
		position = 5
		
		required_missions = {	Z53_fortify_corvurian_border	}
		
		trigger = {
			A59 = {
				exists = yes
				vassal_of = ROOT
			}
		}
		
		effect = {
			add_prestige = 25
			add_country_modifier = {
				name = barumand_benevolent_victor
				duration = 9125
			}
		}
	}
	
	Z53_integrate_corvuria = {
		icon = mission_have_two_subjects
		generic = no
		ai = yes
		position = 6
		
		required_missions = {	Z53_subjugate_corvuria	}
		
		provinces_to_highlight = {
			region = dostanor_region
			NOT = {
				owned_by = ROOT
			}
		}
		
		trigger = {
			dostanor_region = {
				type = all
				owned_by = ROOT
			}
		}
		
		effect = {
			add_accepted_culture = corvurian
			add_country_modifier = {
				name = barumand_human_subjects
				duration = 9125
			}
		}
	}
	
	Z53_secure_daravans_folly = {
		icon = mission_malayan_viceroyalty
		generic = no
		ai = yes
		position = 7
		
		required_missions = {	Z53_integrate_corvuria	}
		
		provinces_to_highlight = {
			region = daravans_folly_region
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			daravans_folly_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT	
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_trade_flow
				duration = 9125
			}
			add_prestige = 10
		}
	}
}

barumand_economic_branch = {
	slot = 3
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		tag = Z53
	}
	
	Z53_grand_gathering = {
		icon = mission_extent_yasak_to_the_east
		generic = no
		ai = yes
		position = 1
		
		trigger = {
			NOT = {
				exists = B21
				exists = B29
			}
			total_development = 150
		}
		
		effect = {
			country_event = { id = flavor_barumand.1 }
		}
	}
	
	Z53_food_for_our_people = {
		icon = mission_rb_import_potatoes
		generic = no
		ai = yes
		position = 2
		
		required_missions = {	Z53_grand_gathering	}
		
		trigger = {
			grain = 5
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_content_population
				duration = 9125
			}
		}
	}
	
	Z53_new_cities = {
		icon = mission_colonial
		generic = no
		ai = yes
		position = 3
		
		required_missions = {	Z53_food_for_our_people	}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 5
				region = south_castanor_region
				has_building = marketplace
			}
			
			num_of_owned_provinces_with = {
				value = 5
				region = south_castanor_region
				has_building = temple
			}
			
			num_of_owned_provinces_with = {
				value = 5
				region = south_castanor_region
				has_building = workshop
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_repopulating_escann
				duration = 9125
			}
		}
	}
	
	Z53_developing_economy = {
		icon = mission_early_game_buildings
		generic = no
		ai = yes
		position = 4
		
		required_missions = {	Z53_new_cities	}
		
		trigger = { share_of_starting_income = 2.5 }
		
		effect = {
			add_country_modifier = {
				name = barumand_building_boom
				duration = 9125
			}
		}
	}
	
	Z53_advanced_farming = {
		icon = mission_dominate_home_trade_node
		generic = no
		ai = yes
		position = 5
		
		required_missions = {	Z53_developing_economy	}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 5
				region = south_castanor_region
				trade_goods = grain
				has_building = farm_estate
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_prospering_farmers
				duration = 9125
			}
		}
	}
	
	Z53_develop_cloth_production = {
		icon = mission_have_manufactories
		generic = no
		ai = yes
		position = 6
		
		required_missions = {	Z53_advanced_farming	}
		
		provinces_to_highlight = {
			owned_by = ROOT
			trade_goods = wool
			NOT = { has_building = textile }
		}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 1
				region = south_castanor_region
				trade_goods = wool
				has_building = textile
				base_production = 5
			}
		}
		
		effect = {
			custom_tooltip = barumand_develop_cloth_production_tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						trade_goods = wool
						has_building = textile
					}
					change_trade_goods = cloth
					add_base_production = 2
				}
			}
		}
	}
	
	Z53_barumandi_abolitionism = {
		icon = mission_rb_monopolize_the_channel
		generic = no
		ai = yes
		position = 7
		
		required_missions = {	Z53_develop_cloth_production	}
		
		provinces_to_highlight = {
			owned_by = ROOT
			trade_goods = slaves
		}
		
		trigger = {
			OR = { 
				year = 1750
				full_idea_group = innovativeness_ideas
			}
			num_of_owned_provinces_with = {
				value = 1
				region = south_castanor_region
				trade_goods = slaves
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_abolitionism
				duration = -1
			}
			custom_tooltip = barumand_abolitionism_tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						trade_goods = slaves
					}
					change_trade_goods = livestock
					remove_building = tradecompany
					add_base_tax = 1
					add_base_production = 1
					add_base_manpower = 2
					if = {
						limit = { has_province_modifier = slave_entrepot }
						remove_province_modifier = slave_entrepot
					}
				}
			}
			add_prestige = 25
		}
	}
}

barumand_development_branch = {
	slot = 4
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		tag = Z53
	}
	
	Z53_gather_funds = {
		icon = mission_war_chest
		generic = no
		ai = yes
		position = 2
		
		required_missions = {	Z53_grand_gathering	}
		
		provinces_to_highlight = {
			province_id = 813
			NOT = { owned_by = ROOT  }
		}
		
		trigger = {
			813 = {
				owned_by = ROOT
			}
			treasury = 250
		}
		
		effect = {
			add_treasury = -250
			813 = {
				add_province_modifier = {
					name = barumand_growing_capital
					duration = 18250
				}
			}
		}
	}
	
	Z53_build_new_capital = {
		icon = mission_colonial
		generic = no
		ai = yes
		position = 3
		
		required_missions = {	Z53_gather_funds	}
		
		provinces_to_highlight = {
			province_id = 813
			OR = {
				NOT = { owned_by = ROOT  }
				NOT = { fort_level = 2 }
				NOT = { development = 10 }
				NOT = { is_capital = no }
			}
		}
		
		trigger = {
			813 = {
				owned_by = ROOT
				development = 10
				fort_level = 2
				is_capital = yes
			}
		}
		
		effect = {
			813 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
				change_province_name = Dustandar
				rename_capital = Dustandar
			}
		}
	}
	
	Z53_build_grand_arena = {
		icon = mission_unite_west_india
		generic = no
		ai = yes
		position = 4
		
		required_missions = {	Z53_build_new_capital	}
		
		provinces_to_highlight = {
			province_id = 803
			NOT = { owned_by = ROOT  }
		}
		
		trigger = {
			813 = {
				owned_by = ROOT
			}
			treasury = 250
			manpower = 5
			mil_power = 150
		}
		
		effect = {
			add_treasury = -250
			add_manpower = -5
			add_mil_power = -150
			813 = {
				add_base_manpower = 3
				add_province_modifier = {
					name = barumand_dustandar_grand_arena
					duration = -1
				}
			}
		}
	}
	
	Z53_development_of_the_capital = {
		icon = mission_subjugate_saurashtra
		generic = no
		ai = yes
		position = 5
		
		required_missions = {	Z53_build_grand_arena	}
		
		provinces_to_highlight = {
			province_id = 813
			OR = {
				NOT = { owned_by = ROOT  }
				NOT = { 
					OR = {
						has_building = temple
						has_building = cathedral
					}
				}
				NOT = { 
					OR = {
						has_building = marketplace
						has_building = trade_depot
					}
				}
				NOT = { 
					OR = {
						has_building = workshop
						has_building = counting_house
					}
				}
				NOT = { development = 25 }
			}
		}
		
		trigger = {
			813 = {
				owned_by = ROOT
				development = 25
				OR = {
					has_building = temple
					has_building = cathedral
				}
				OR = {
					has_building = marketplace
					has_building = trade_depot
				}
				OR = {
					has_building = workshop
					has_building = counting_house
				}
			}
		}
		
		effect = {
			add_adm_power = 250
		}
	}
	
	Z53_beacon_of_knowledge = {
		icon = mission_rb_renovate_oxbridge
		generic = no
		ai = yes
		position = 6
		
		required_missions = {	Z53_development_of_the_capital	}
		
		provinces_to_highlight = {
			province_id = 813
			OR = {
				NOT = { owned_by = ROOT  }
				NOT = { has_building = university }
				NOT = { development = 41 }
			}
		}
		
		trigger = {
			813 = {
				owned_by = ROOT
				development = 41
				has_building = university
				NOT = { num_free_building_slots = 1 }
			}
		}
		
		effect = {
			add_country_modifier = {
				name = barumand_beacon_of_knowledge
				duration = -1
			}
		}
	}
}

barumand_diplomatic_branch = {
	slot = 5
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		tag = Z53
	}
	
	Z53_orcish_alliances = {
		icon = mission_alliances
		generic = no
		ai = yes
		position = 1
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = our_green_kin_tooltip
				num_of_allies = 2
				calc_true_if = {
					all_ally = {
						primary_culture = green_orc
						has_opinion = {
							who = ROOT
							value = 150
						}
					}
					amount = 2
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = "influential_diplomacy"
				duration = 9125 #25 years
			}
		}
	}
	
	Z53_restore_control_over_goblins = {
		icon = mission_have_two_subjects
		generic = no
		ai = yes
		position = 2
		
		required_missions = {	Z53_orcish_alliances	}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = barumand_restore_control_over_goblins_tooltip
				num_of_vassals = 1
				calc_true_if = {
					all_subject_country = {
						culture_group = goblinoid
					}
					amount = 1
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = "obedient_subjects"
				duration = 9125 #25 years
			}
			add_dip_power = 100
		}
	}
	
	Z53_civilized_diplomacy = {
		icon = mission_colonial
		generic = no
		ai = yes
		position = 3
		
		required_missions = {	Z53_restore_control_over_goblins	}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = barumand_civilized_diplomacy_tooltip
				num_of_allies = 1
				calc_true_if = {
					any_ally = {
						OR = {
							NOT = { culture_group = orcish }
							NOT = { culture_group = goblinoid }
						}
					}
					amount = 1
				}
			}
		}
		
		effect = {
			add_prestige = 10
			add_country_modifier = {
				name = "influential_diplomacy"
				duration = 9125 #25 years
			}
		}
	}
}

barumand_mint = {
	slot = 5
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		tag = Z53
	}
	
	Z53_barumandi_mint = {
		icon = mission_african_gold
		generic = no
		ai = yes
		position = 4
		
		required_missions = {	Z53_build_new_capital	}
		
		provinces_to_highlight = {
			owned_by = ROOT
			trade_goods = gold
			OR = {	
				NOT = { development = 16 }
				NOT = { base_production = 8 }
			}
		}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 1
				region = south_castanor_region
				trade_goods = gold
				development = 16
				base_production = 8
			}
		}
		
		effect = {
			add_treasury = 500
			add_inflation = 3
			813 = {
				add_province_modifier = {
					name = barumand_dustandar_mint
					duration = -1
				}
			}
		}
	}
}