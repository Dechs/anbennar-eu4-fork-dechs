#63 - Erfurt

owner = A08
controller = A08
add_core = A08
culture = visfoot_halfling
religion = regent_court
hre = no
base_tax = 10
base_production = 10
trade_goods = dyes
center_of_trade = 2
base_manpower = 6
capital = "South Viswall"
is_city = yes
fort_15th = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

extra_cost = 10

