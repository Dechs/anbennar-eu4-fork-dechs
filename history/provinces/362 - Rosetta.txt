#362 - Delta

owner = A29
controller = A29
add_core = A29
culture = busilari
religion = regent_court

hre = no

base_tax = 4
base_production = 3
base_manpower = 3

trade_goods = cloth

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
