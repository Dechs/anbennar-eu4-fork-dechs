# No previous file for Banten
owner = F48
controller = F48
add_core = F48
add_core = F50
culture = siadunan_harpy
religion = the_hunt

hre = no

base_tax = 1
base_production = 1
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari