# No previous file for Zaporozhia
owner = A33
controller = A33
add_core = A33
culture = vernman
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish

add_permanent_province_modifier = {
	name = dwarven_minority_coexisting_large
	duration = -1
}