country_decisions = {

	dostanorian_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			was_never_end_game_tag_trigger = yes
			NOT = { has_country_flag = formed_dostanor }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			NOT = { exists = Z07 }
			culture_group = dostanorian	
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 20
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					ai = no
					is_former_colonial_nation = yes
				}
			}
		}
		provinces_to_highlight = {
			OR = {
				province_id = 447 #Corveld
				province_id = 510 #Bal Ouord
				province_id = 441 #Arca Corvur
				province_id = 427 #Ioan's Ford
				province_id = 432 #Cannmarionn
				province_id = 520 #Akrad-til
				province_id = 514 #Marwar
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_at_war = no
			AND = {
				owns_core_province = 447 #Corveld 
#				NOT = { 
#					447 = { has_province_modifier = ruined_city }
#				}
			}	
			AND = {
				owns_core_province = 510 #Bal Ouord
#				510 = { has_province_modifier = castanorian_citadel } 
			}	
			owns_core_province = 441 #Arca Corvur
			owns_core_province = 427 #Ioan's Ford
			owns_core_province = 432 #Cannmarionn
			owns_core_province = 520 #Akrad-til
			owns_core_province = 514 #Marwar
		}
		effect = { 
			change_tag = Z07
			swap_non_generic_missions = yes
#			if = {
#				limit = {
#					is_part_of_hre = yes
#					is_elector = no
#					is_emperor = no
#				}
#				emperor = {
#					add_opinion = {
#						who = root
#						modifier = opinion_left_empire
#					}
#				}
#				set_in_empire = no
#				emperor = {
#					country_event = { id = hre_event.5 }
#				}
#			}
			if = {
				limit = {
					NOT = { government_rank = 2 }
				}
				set_government_rank = 2
			}	
			daravans_folly_region = {
				limit = {
					NOT = { owned_by_by = ROOT }
				}
				add_permanent_claim = Z07
			}	
			dostanor_region = {
				limit = {
					NOT = { owned_by_by = ROOT }
				}
				add_permanent_claim = Z07
			}	
			ourdia_region = {
				limit = {
					NOT = { owned_by_by = ROOT }
				}
				add_permanent_claim = Z07
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			
			add_prestige = 25
#			Z07 = {
#				set_capital = 447
#			}
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			set_country_flag = formed_dostanor
		}
		ai = {
			factor = 1
			modifier = {
				factor = 0
				government = republic
				}
			}
		}
	}
}