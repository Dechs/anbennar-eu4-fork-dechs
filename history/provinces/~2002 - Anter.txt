# No previous file for Galápagos Islands
owner = H14
controller = H14
add_core = H14
culture = snecboth
religion = eordellon
capital = ""
fort_15th = yes

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = naval_supplies

native_size = 14
native_ferocity = 6
native_hostileness = 6