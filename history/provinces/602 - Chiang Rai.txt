# No previous file for Chiang Rai
owner = F39
controller = F39
add_core = F39
culture = zanite
religion = bulwari_sun_cult

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari


add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}